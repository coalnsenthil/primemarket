//
//  FreindsOnMarketVC.swift
//  SMarket
//
//  Created by CIPL0668 on 11/09/20.
//  Copyright © 2020 Mind. All rights reserved.
//

import UIKit

class FriendsOnSmarketCell: UITableViewCell {
    
    @IBOutlet weak var lblName : UILabel!
    @IBOutlet weak var lblPhone : UILabel!
    @IBOutlet weak var lblEarns: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
class FreindsOnMarketVC: ParentViewController {
    @IBOutlet weak var tableRefered: UITableView!
    
    var arrRefered = [Any]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initialize()
    }
    
    fileprivate func initialize()  {
        self.title = "FRIENDS ON SMARKET"
        getRefered()
    }
}
// MARK:-
// MARK:- UITableViewDelegate, UITableViewDataSource

extension FreindsOnMarketVC : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrRefered.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "FriendsOnSmarketCell") as? FriendsOnSmarketCell{
            
            if let data = arrRefered[indexPath.row] as? [String : Any] {
                cell.lblName.text = data.valueForString(key: "name")
                cell.lblPhone.text = data.valueForString(key: "mobile_number")
            }
            cell.lblEarns.attributedText = attributedEarns()
            //  cell.lblName.text = data.valueForString(key: "name")
            return cell
        }else{
            return UITableViewCell()
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    func attributedEarns() -> NSMutableAttributedString{
        
        //        let strSignUp = "NEW TO SMARKET? SIGN UP"
        let strSignUp = "Earns you 30%"
        
        let attributedString = NSMutableAttributedString(string: strSignUp)
        
        attributedString.setAttributes([NSAttributedStringKey.foregroundColor : ColorBlack_000000,                                      NSAttributedStringKey.font : UIFont.systemFont(ofSize: 14, weight: .regular)], range: NSRange(location: 0, length: strSignUp.count))
        
        attributedString.setAttributes([NSAttributedStringKey.foregroundColor : ColorRedExpireDate,NSAttributedStringKey.font :
            UIFont.systemFont(ofSize: 16, weight: .semibold)],
                                       range:strSignUp.rangeOf("30%"))
        
        return attributedString
    }
}
extension FreindsOnMarketVC {
    fileprivate func getRefered(){
        
        let param = ["user_id": appDelegate?.loginUser?.user_id]
        
        if arrRefered.count == 0  {
            tableRefered.startLoadingAnimation(tintColor: .gray, backgroundColor: .white)
        }
        
        APIRequest.shared().customerReferred(param as [String : Any]) { (response, error) in
            
            if let json = response as? [String : Any], let data = json[CJsonData] as? [String : Any] {
                if let refer = data.valueForJSON(key: "refered"){
                    self.arrRefered = refer as! [[String : Any]]
                    self.tableRefered.reloadData()
                }
            }
            
            if self.arrRefered.count > 0 {
                self.tableRefered.stopLoadingAnimation()
                
            } else if error == nil {
                self.tableRefered.showDataStatusView(status: .noResultFound, tintColor: .gray, backgroundColor: .clear, tapToRetry: nil)
                
            } else if let error = error as NSError? {
                
                self.tableRefered.showDataStatusView(status: (error.code == -1001 || error.code == -1009) ? .noInternet : .other, tintColor: .gray, backgroundColor: .clear, tapToRetry: {
                    self.getRefered()
                })
            }
        }
    }
}
