//
//  StoreCreditViewController.swift
//  SMarket
//
//  Created by Mac-00014 on 07/07/18.
//  Copyright © 2018 Mind. All rights reserved.
//

import UIKit

class StoreCreditViewController: ParentViewController {
    
    @IBOutlet weak var vwGenerateRedemptionCode :UIView!
    @IBOutlet weak var imgVIcon :UIImageView!
    @IBOutlet weak var lblMerchantName : UILabel!
    @IBOutlet weak var lblTagline : UILabel!
    @IBOutlet weak var lblTotalCredit : UILabel!
    @IBOutlet weak var lblCurrency : UILabel!
    @IBOutlet weak var txtCredit : UITextField!
    @IBOutlet weak var btnGenerateCode : UIButton!
    
    @IBOutlet weak var tblView: UITableView!
    var arrCredit = [Any]()
    var page = 1
    var selectedMerchant : Merchant?
    var apiSessionTask : URLSessionTask?
    //MARK:-
    //MARK:- LIFE CYCLE
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initialize()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        
    }
    
    // MARK:-
    // MARK:- GENERAL METHODS
    fileprivate func initialize()  {
        self.title = "STORE CREDITS"
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image:#imageLiteral(resourceName: "search"), style: .plain, target: self, action: #selector(btnSearchClicked))
    
        redemptionPopUpConfiguration()
        
        //.. refreshControl
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(pullToRefresh), for: .valueChanged)
        refreshControl.tintColor = .gray
        tblView?.pullToRefreshControl = refreshControl

        loadStoreCreditListFromServer()
        
        //.. Enable location service and update api
        var isNeedRefresh = true
        appDelegate?.enableLocationService({ (status, location) in
            
            if  status == .authorizedWhenInUse && location != nil && isNeedRefresh {
                isNeedRefresh = false
                self.pullToRefresh()
            }else if status == .restricted || status == .denied{
                isNeedRefresh = true
                self.pullToRefresh()
            }
        })
    }
    
    
    // MARK:-
    // MARK:- ACTION EVENT
    
    @objc fileprivate func btnSearchClicked(_ sender : UIBarButtonItem) {
        
        if let searchVC = CMainCustomer_SB.instantiateViewController(withIdentifier: "MerchantSearchViewController") as? MerchantSearchViewController {
            self.navigationController?.pushViewController(searchVC, animated: true)
        }
        
    }
}

// MARK:-
// MARK:- Redemption popUp Configuration

extension StoreCreditViewController {
    
    func redemptionPopUpConfiguration()  {
        
        vwGenerateRedemptionCode.CViewSetWidth(width: 335/375*CScreenWidth)
        vwGenerateRedemptionCode.CViewSetHeight(height: 315/375*CScreenWidth)
        vwGenerateRedemptionCode.layer.cornerRadius = 5
        vwGenerateRedemptionCode.layer.masksToBounds = true
        txtCredit.layer.cornerRadius = 10
        txtCredit.layer.masksToBounds = true
        btnGenerateCode.layer.cornerRadius = 10
        btnGenerateCode.layer.masksToBounds = true
        txtCredit.addLeftImage(strImgName: nil, padding: 36, imageContentMode: .Center)
    }
    
    func openRedemptionPopUp(_ indexPath : IndexPath)  {
        
        txtCredit.text = ""
        if let data = arrCredit[indexPath.row] as? [String : Any] {
            selectedMerchant = Merchant(object: data)
            
            imgVIcon.imageWithUrl(self.selectedMerchant?.logo)
            lblMerchantName.text = self.selectedMerchant?.name
            lblTagline.text = self.selectedMerchant?.tagLine
            lblTotalCredit.text = "You have \(appDelegate!.currency) \(selectedMerchant?.storeCredit ?? "0") available as the usable store credit. Enter store credit redemption amount in below here to redeem store credit."
            
            self.presentPopUp(view: vwGenerateRedemptionCode, shouldOutSideClick: false, type: .center) {
                
            }
        }
    }
    
    @IBAction fileprivate func btnCloseClicked(_ sender:UIButton) {
        self.dismissPopUp(view: vwGenerateRedemptionCode) {  }
    }
    
    @IBAction fileprivate func btnGenerateCodeClicked(_ sender:UIButton) {
        
        if (txtCredit.text?.isBlank)! {
            
            self.presentAlertViewWithOneButton(alertTitle: "", alertMessage: CMessageBlankStoreCredit, btnOneTitle: CBtnOk) { (action) in
            }
        } else {
            if (txtCredit.text?.toInt)! <= (selectedMerchant?.storeCredit?.toInt)! {
                self.dismissPopUp(view: vwGenerateRedemptionCode) {
                     self.generateRedemption()
                }
            } else {
                self.presentAlertViewWithOneButton(alertTitle: "", alertMessage: CMessageBlankStoreCredit, btnOneTitle: CBtnOk) { (action) in
                }
            }
        }
    }
}

// MARK:-
// MARK:- Server Request

extension StoreCreditViewController {
    
    @objc func pullToRefresh()  {
        
        page = 1
        loadStoreCreditListFromServer()
    }
    
    fileprivate func loadStoreCreditListFromServer() {
        
        if apiSessionTask != nil && apiSessionTask?.state == .running {
            apiSessionTask?.cancel()
        }
        
        var param = [String : Any]()
        param["page"] = page
        
        if page == 1 && arrCredit.count == 0 {
            if let refreshController = tblView.refreshControl, !refreshController.isRefreshing {
                tblView.startLoadingAnimation(tintColor: .gray, backgroundColor: .white)
            }
        }
        
      apiSessionTask = APIRequest.shared().loadStoreCredit(param) { (response, error) in
            self.tblView.pullToRefreshControl?.endRefreshing()
            
            if APIRequest.shared().isJSONStatusValid(withResponse: response) {
                
                if let dataResponse = response as? [String : Any], let data = dataResponse[CJsonData] as? [[String: Any]], data.count > 0 {
                    
                    if self.page == 1 {
                        self.arrCredit.removeAll()
                        self.tblView.reloadData()
                    }
                    
                    self.arrCredit = self.arrCredit + data
                    self.page = self.page + 1
                    self.tblView.reloadData()
                    
                }else {
                    self.page = 0
                }
            }
            
            //For remove loader or display data not found
            if self.arrCredit.count > 0 {
                self.tblView.stopLoadingAnimation()
                
            } else if error == nil {
                self.tblView.showDataStatusView(status: .noResultFound, tintColor: .gray, backgroundColor: .clear, tapToRetry: nil)
                
            } else if let error = error as NSError? {
                
                // ... -999 cancelled api
                // ... --1001 or -1009 no internet connection
                
                if error.code != -999 {
                    
                    self.tblView.showDataStatusView(status: (error.code == -1001 || error.code == -1009) ? .noInternet : .other, tintColor: .gray, backgroundColor: .clear, tapToRetry: {
                        
                        self.pullToRefresh()
                        
                    })
                }
            }
        }
    }
    fileprivate func generateRedemption() {
        
        var param = [String : Any]()
        param["id"] = self.selectedMerchant?.id
        param["amount"] = self.txtCredit.text
        
        APIRequest.shared().generateRedemptionCode(param) { (response, error) in
            
            if APIRequest.shared().isJSONDataValid(withResponse: response) {
                
                if let json = response as? [String : Any], let meta = json[CJsonMeta] as? [String : Any],let data = json[CJsonData] as? [String : Any] {
                    
                    self.showAlertView(meta.valueForString(key: CJsonMessage), completion: { (action) in
                        if let offerDetailsVC = CMainCustomer_SB.instantiateViewController(withIdentifier: "QRCodeDetailsViewController") as? QRCodeDetailsViewController {
                            offerDetailsVC.iObject = data
                            offerDetailsVC.qrDetailsType = .StoreCreditOffer
                            self.navigationController?.pushViewController(offerDetailsVC, animated: true)
                        }
                    })
                }
            }
        }
    }
}

// MARK:-
// MARK:- UITableViewDelegate, UITableViewDataSource

extension StoreCreditViewController : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrCredit.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell = tableView.dequeueReusableCell(withIdentifier: "MerchantListTableViewCell") as? MerchantListTableViewCell  {
            
            if let data = arrCredit[indexPath.row] as? [String : Any] {
                let merchant = Merchant(object: data)
                
                cell.lblMerchantName.text = merchant.name
                cell.lblSubTitle.text = merchant.tagLine
                cell.lblAddress.text = merchant.address
                cell.lblStoreCredits.text = "\(appDelegate!.currency)\(merchant.storeCredit ?? "0")"
                cell.lblDistance.text = "(\(merchant.distance ?? "0") mi)"
                
                cell.imgVIcon.imageWithUrl(merchant.logo)
                cell.imgVIcon.touchUpInside { (imageView) in
                    self.fullScreenImage(imageView, urlString: merchant.logo)
                }
                cell.btnWeb.hide(byWidth: (merchant.website?.isBlank)!)
                cell.btnWeb.touchUpInside { (sender) in
                    self.openInSafari(strUrl: merchant.website)
                }
                cell.btnCall.hide(byWidth: (merchant.mobile?.isBlank)!)
                cell.btnCall.touchUpInside { (sender) in
                    if let code = merchant.countryCode, let mobileNo = merchant.mobile {
                        self.openPhoneDialer(code + mobileNo)
                    }
                }
                cell.btnDirection.touchUpInside { (sender) in
                    self.openGoogleMap(merchant.latitude, longitude: merchant.longitude, address: merchant.address)
                }
            }
            
            if indexPath.row == arrCredit.count - 1 && page != 0 {
                self.loadStoreCreditListFromServer()
            }
            
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        openRedemptionPopUp(indexPath)
        
    }
}
