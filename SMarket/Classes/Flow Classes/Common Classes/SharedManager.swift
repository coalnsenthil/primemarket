//
//  SharedManager.swift
//  SMarket
//
//  Created by CIPL0668 on 11/09/20.
//  Copyright © 2020 Mind. All rights reserved.
//

import UIKit

class SharedManager{
    
    static let shared = SharedManager()
    var isGoogleSignin = false
    private init() { }
}
