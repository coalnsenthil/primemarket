//
//  Offer.swift
//  SMarket
//
//  Created by Mac-00016 on 10/08/18.
//  Copyright © 2018 Mind. All rights reserved.
//

import Foundation

public final class Offer {
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
       
        static let offer_type = "offer_type"
        static let expiry_date = "expiry_date"
        static let sub_offer = "sub_offer"
        static let message = "message"
        static let id = "id"
        static let referral_count = "referral_count"
    }
    
    // MARK: Properties
    public var offerType: OfferType?
    public var offerTitle: String?
    public var expiryDate: String?
    public var message: String?
    public var subOffer: [SubOffer]?
    public var id: String?
    public var referral_count: String?
    
    // MARK: SwiftyJSON Initializers
    /// Initiates the instance based on the object.
    ///
    /// - parameter object: The object of either Dictionary or Array kind that was passed.
    /// - returns: An initialized instance of the class.
    public convenience init(object: Any) {
        self.init(json: JSON(object))
    }
    
    /// Initiates the instance based on the JSON that was passed.
    ///
    /// - parameter json: JSON object from SwiftyJSON.
    public required init(json: JSON) {
        self.updateJSON(json: json)
    }
    
    
    func updateValues(object: Any) {
        self.updateJSON(json: JSON(object))
    }
    
    private func updateJSON(json: JSON) {
        
        offerType = OfferType(rawValue: json[SerializationKeys.offer_type].intValue)
        expiryDate = json[SerializationKeys.expiry_date].stringValue
        message = json[SerializationKeys.message].stringValue
        referral_count = json[SerializationKeys.referral_count].stringValue
        
        if json[SerializationKeys.id].error != .notExist  {
            id  = json[SerializationKeys.id].stringValue
        }
        
        offerTitle = offerType == .Referral ? "Referral Offer" : "Rate & Review"
        
        if let arrObject = json[SerializationKeys.sub_offer].arrayObject, arrObject.count > 0  {
            subOffer = []
            for item in arrObject {
                let sub_Offer = SubOffer(object: item)
                subOffer?.append(sub_Offer)
            }
        }
    }
}
