//
//  ReferralAlertsViewController.swift
//  SMarket
//
//  Created by Mac-00014 on 05/07/18.
//  Copyright © 2018 Mind. All rights reserved.
//

import UIKit

class ReferralAlertsViewController: ParentViewController {

    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var lblRefAlerts: GenericLabel!
    var apiSessionTask : URLSessionTask?
    var arrAlerts = [Any]()
    var page = 1
    
    //MARK:-
    //MARK:- LIFE CYCLE
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initialize()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        lblRefAlerts.text = "\(appDelegate?.loginUser?.refferal_alert ?? "0")"
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        
    }
    
    // MARK:-
    // MARK:- GENERAL METHODS
    fileprivate func initialize()  {
        self.title = "REFERRAL ALERTS"
        
        
        if let viewController = self.navigationController?.viewControllers {
            
            if !viewController.contains(where: { return $0 is MerchantSearchViewController }) {
                self.navigationItem.rightBarButtonItem = UIBarButtonItem(image:#imageLiteral(resourceName: "search"), style: .plain, target: self, action: #selector(btnSearchClicked))
            }
        }else {
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(image:#imageLiteral(resourceName: "search"), style: .plain, target: self, action: #selector(btnSearchClicked))
        }
        
        
        loadReferralAlertListFromServer()
        
        //.. refreshControl
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(pullToRefresh), for: .valueChanged)
        refreshControl.tintColor = .gray
        tblView?.pullToRefreshControl = refreshControl
        
    
        //.. Enable location service and update api
        var isNeedRefresh = true
        appDelegate?.enableLocationService({ (status, location) in
            
            if  status == .authorizedWhenInUse && location != nil && isNeedRefresh {
                isNeedRefresh = false
                self.pullToRefresh()
            }else if status == .restricted || status == .denied{
                isNeedRefresh = true
                self.pullToRefresh()
            }
        })
        
    }
    
    // MARK:-
    // MARK:- ACTION EVENT
    
    @objc fileprivate func btnSearchClicked(_ sender : UIBarButtonItem) {
        
        if let searchVC = CMainCustomer_SB.instantiateViewController(withIdentifier: "MerchantSearchViewController") as? MerchantSearchViewController {
            self.navigationController?.pushViewController(searchVC, animated: true)
        }
    }
}

// MARK:-
// MARK:- Server Request

extension ReferralAlertsViewController {
    
    @objc func pullToRefresh()  {
        
        page = 1
        loadReferralAlertListFromServer()
    }
    
    fileprivate func loadReferralAlertListFromServer() {
        
        if apiSessionTask != nil && apiSessionTask?.state == .running {
            apiSessionTask?.cancel()
        }
        
        var param = [String : Any]()
        param["page"] = page
        
        if let id = iObject as? String {
            param["merchant_id"] = id
        }
        
        if page == 1 && arrAlerts.count == 0 {
            if let refreshController = tblView.refreshControl, !refreshController.isRefreshing {
                tblView.startLoadingAnimation(tintColor: .gray, backgroundColor: .white)
            }
        }
        
        apiSessionTask = APIRequest.shared().loadReferralAlerts(param) { (response, error) in
            self.tblView.pullToRefreshControl?.endRefreshing()
            
            if APIRequest.shared().isJSONStatusValid(withResponse: response) {
                
                if self.page == 1 {
                    self.arrAlerts.removeAll()
                    self.tblView.reloadData()
                }
                
                if let dataResponse = response as? [String : Any], let data = dataResponse[CJsonData] as? [[String: Any]], data.count > 0 {
                    
                    self.arrAlerts = self.arrAlerts + data
                    self.page = self.page + 1
                    self.tblView.reloadData()
                }
                else {
                    self.page = 0
                }
            }
            
            //For remove loader or display data not found
            if self.arrAlerts.count > 0 {
                self.tblView.stopLoadingAnimation()
                
            } else if error == nil {
                self.tblView.showDataStatusView(status: .noResultFound, tintColor: .gray, backgroundColor: .clear, tapToRetry: nil)
                
            } else if let error = error as NSError? {
                
                // ... -999 cancelled api
                // ... --1001 or -1009 no internet connection
                
                if error.code != -999 {
                    
                    self.tblView.showDataStatusView(status: (error.code == -1001 || error.code == -1009) ? .noInternet : .other, tintColor: .gray, backgroundColor: .clear, tapToRetry: {
                        
                        self.pullToRefresh()
                        
                    })
                }
            }
        }
    }
}

// MARK:-
// MARK:- UITableViewDelegate, UITableViewDataSource

extension ReferralAlertsViewController : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrAlerts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell = tableView.dequeueReusableCell(withIdentifier: "ReferralAlertTableViewCell") as? ReferralAlertTableViewCell  {
        
            if let data = arrAlerts[indexPath.row] as? [String : Any] {
                
                let merchant = OfferList(object: data)
                
                cell.lblMerchantName.text = merchant.name
                cell.lblSubTitle.text = merchant.tagLine
                cell.lblReviews.text = "(\(merchant.noOfRating ?? "0"))"
                cell.lblRatingCount.text = "\(merchant.avgRating ?? 0.0)"
                cell.vWRatingV.setRating(merchant.avgRating ?? 0.0)
                cell.lblDistance.text = "(\(merchant.distance ?? "0.0") mi)"
                cell.lblStoreCredits.text = "\(currencyUnit)\(merchant.storeCredit ?? "0")"
                cell.lblReferrals.text = merchant.referrals
                cell.btnOfferType.setImage(merchant.exclusiveImage, for: .normal)
                cell.lblOfferType.text = merchant.categoryName
                
                if merchant.status == .Expire {
                    cell.lblExpires.textColor = merchant.status == .Expire ? ColorRedExpireDate : ColorBlack_000000
                    cell.lblExpires.text = "Expired on: \(merchant.expiryDate?.dateFromString ?? "-")"
                } else if merchant.status == .Pending {
                    cell.lblExpires.text = "Expires on: \(merchant.expiryDate?.dateFromString ?? "-")"
                } else{
                    cell.lblExpires.text = ""
                }
                if merchant.status == .Redeemed {
                    cell.lblRedeemDate.text = "Redeemed on: \(merchant.redeemedDate?.dateFromString ?? "-")"
                } else {
                    cell.lblRedeemDate.text = ""
                }

                cell.imgVIcon.imageWithUrl(merchant.logo)
                cell.imgVIcon.touchUpInside { (imageView) in
                    self.fullScreenImage(imageView, urlString: merchant.logo)
                }
                
                switch merchant.subOfferCategory {
                case .GiftCard? :
                    cell.lblOfferValue.text = "\(currencyUnit)\(merchant.amount ?? "0")"
                    cell.lblPurchaseBy.text = merchant.title
                    
                case .InStore? :
                    cell.lblOfferValue.text = ""
                    cell.lblPurchaseBy.text = merchant.title
                    
                case .StoreCredit? :
                    cell.lblOfferValue.text = "\(currencyUnit)\(merchant.amount ?? "0")"
                    cell.lblPurchaseBy.text = merchant.purchasedBy
                default:
                    break
                }
                
            }
            if indexPath.row == arrAlerts.count - 1 && page != 0 {
                self.loadReferralAlertListFromServer()
            }
            
            return cell
        }
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if let offerDetailsVC = CMainCustomer_SB.instantiateViewController(withIdentifier: "OfferDetailsViewController") as? OfferDetailsViewController {
            offerDetailsVC.iObject = arrAlerts[indexPath.row]
            self.navigationController?.pushViewController(offerDetailsVC, animated: true)
        }
    }
}

