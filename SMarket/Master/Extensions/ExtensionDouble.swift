//
//  ExtensionDouble.swift
//  Master
//
//  Created by Mac-00014 on 06/06/18.
//  Copyright © 2018 MindInventory. All rights reserved.
//

import Foundation
import UIKit

extension Double {
    
    var toInt:Int? {
        return Int(self)
    }
    
    var toFloat:Float? {
        return Float(self)
    }
    
    var toString:String {
        return "\(self)"
    }
    
}
