//
//  MerchantDetailsTabCollectionViewCell.swift
//  SMarket
//
//  Created by Mac-00014 on 04/09/18.
//  Copyright © 2018 Mind. All rights reserved.
//

import UIKit

class MerchantDetailsTabCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var vwContainer : UIView!
    @IBOutlet weak var btnType : UIButton!
    @IBOutlet weak var lblTitle : UILabel!
    
}
