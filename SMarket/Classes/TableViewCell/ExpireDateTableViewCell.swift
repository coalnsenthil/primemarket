//
//  ExpireDateTableViewCell.swift
//  SMarket
//
//  Created by Mac-00014 on 17/07/18.
//  Copyright © 2018 Mind. All rights reserved.
//

import UIKit

class ExpireDateTableViewCell: UITableViewCell {
    
    @IBOutlet weak var lblExpireDate : UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
