//
//  MerchantLoginViewController.swift
//  SMarket
//
//  Created by Mac-00014 on 19/06/18.
//  Copyright © 2018 Mind. All rights reserved.
//

import UIKit
import GoogleSignIn
class MerchantLoginViewController: ParentViewController {
    
    @IBOutlet var txtEmail : UITextField!
    @IBOutlet var txtPassword : UITextField!
    @IBOutlet var btnSignUp : UIButton!
    
    //MARK:-
    //MARK:- LIFE CYCLE
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initialize()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        appDelegate?.isCustomerLogin = false
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        
    }
    
    // MARK:-
    // MARK:- GENERAL METHODS
    fileprivate func initialize()  {
        
        txtEmail.text = CUserDefaults.value(forKey: UserDefaultMerchantLoginEmail) as? String ?? ""
        attributedSignUp()
    }
    
    // MARK:-
    // MARK:- ACTION EVENT
    
    @IBAction private func btnLoginClicked(_ sender : UIButton) {
        
        if isValidationPassed() {
            loginWithUserInfo()
        }
    }
    @IBAction private func btnSignUpClicked(_ sender : UIButton) {
        
        if let signUpVC = CLRF_SB.instantiateViewController(withIdentifier: "RegisterViewController") as? RegisterViewController {
            self.navigationController?.pushViewController(signUpVC, animated: true)
        }
    }
    @IBAction private func btnForgotPasswordClicked(_ sender : UIButton) {
        
        if let forgotVC = CLRF_SB.instantiateViewController(withIdentifier: "MerchantForgotPasswordViewController") as? MerchantForgotPasswordViewController {
            self.navigationController?.pushViewController(forgotVC, animated: true)
        }
    }
    
    @IBAction func btnSigninGoogleClick(_ sender: Any) {
        SharedManager.shared.isGoogleSignin = true
        googleLogin()
    }
    
}

// MARK:-
// MARK:- Google sign in

extension MerchantLoginViewController : GIDSignInDelegate {
    
    fileprivate func googleLogin() {
        
        UIApplication.shared.beginIgnoringInteractionEvents()
        GIDSignIn.sharedInstance()?.presentingViewController = self
        GIDSignIn.sharedInstance()?.delegate = self
        GIDSignIn.sharedInstance()?.signIn()
        
    }
    
    func sign(_ signIn: GIDSignIn!, present viewController: UIViewController!) {
        UIApplication.shared.endIgnoringInteractionEvents()
        self.present(viewController, animated: true, completion: nil)
    }
    
    func sign(_ signIn: GIDSignIn!, dismiss viewController: UIViewController!) {
        self.dismiss(animated: true, completion: nil)
        
    }
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        UIApplication.shared.endIgnoringInteractionEvents()
        
        guard error == nil else {
            print("\(error.localizedDescription)")
            return
        }
        
        let param = ["email": user.profile.email!,
                     "socialId": user.userID!,
                     "type":"2",
                     "imageUrl":user?.profile.imageURL(withDimension: 600).absoluteString ?? ""] as [String : Any]
        
        loginWithGoogle(param)
    }
}

// MARK:-
// MARK:- Server Request

extension MerchantLoginViewController {
    
    fileprivate func loginWithGoogle(_ data : [String : Any]?) {
        
        if let userInfo = data {
            
            let param = ["email": userInfo.valueForString(key: "email"),
                         "google_id": userInfo.valueForString(key: "socialId")]
            
            APIRequest.shared().loginWithSocial(param) { (response, error) in
                
                if APIRequest.shared().isJSONDataValid(withResponse: response) {
                    
                    if let json = response as? [String : Any], let meta = json[CJsonMeta] as? [String : Any] {
                        
                        if meta.valueForInt(key: CJsonStatus) == CStatusZero { // Login to user
                            
                            APIRequest.shared().saveLoginUserToLocal(responseObject: response as! [String : AnyObject])
                            appDelegate?.signInMerchantUser(animated: true)
                            
                        } else {
                            
                            if let signUpVC = CLRF_SB.instantiateViewController(withIdentifier: "RegisterViewController") as? RegisterViewController {
                                signUpVC.iObject = userInfo
                                self.navigationController?.pushViewController(signUpVC, animated: true)
                            }
                        }
                    }
                }
            }
        }
    }
    
    fileprivate func loginWithUserInfo() {
        
        let param = ["email": txtEmail.text?.trim ?? ""]
        
        APIRequest.shared().loginWithNormal(param) { (response, error) in
            if APIRequest.shared().isJSONDataValid(withResponse: response) {
                
                if let json = response as? [String : Any], let meta = json[CJsonMeta] as? [String : Any] {
                    
                    switch meta.valueForInt(key: CJsonStatus) {
                        
                    case CStatusZero:
                        
                        if let data = json[CJsonData] as? [String : Any] {
                            CUserDefaults.setValue(data.valueForString(key: "id"), forKey: UserDefaultLoginUserID)
                        }
                        
                        CUserDefaults.setValue(meta.valueForString(key: "token"), forKey: UserDefaultLoginUserToken)
                        CUserDefaults.synchronize()
                        if let verifyEmailVC = CLRF_SB.instantiateViewController(withIdentifier: "VerifyEmailViewController") as? VerifyEmailViewController {
                            verifyEmailVC.iObject = param
                            verifyEmailVC.fromVC = "LOGIN"
                            self.present(UINavigationController(rootViewController: verifyEmailVC), animated: true, completion: nil)
                        }
                        
                       // appDelegate?.signInMerchantUser(animated: true)
                        
                    case CStatusFour:
                        
                        self.presentAlertViewWithTwoButtons(alertTitle: "", alertMessage: meta.valueForString(key: "message"), btnOneTitle: CBtnOk, btnOneTapped: { (action) in
                            
                        }, btnTwoTitle: "Verify Now", btnTwoTapped: { (action) in
                            self.resendOTPEmail()
                        })
                        
                    default:
                        self.showAlertView(meta.valueForString(key: CJsonMessage), completion: nil)
                    }
                    
                }
            }
        }
    }
    
    fileprivate func resendOTPEmail() {
        
        let param = ["email": txtEmail.text!.trim] as [String : Any]
        
        APIRequest.shared().resendEmailOTP(param) { (response, error) in
            if APIRequest.shared().isJSONStatusValid(withResponse: response) {
                
                if let json = response as? [String : Any], let meta = json[CJsonMeta] as? [String : Any] {
                    
                    if meta.valueForInt(key:CJsonStatus) == CStatusZero {
                        self.showAlertView(CMessageOTPResend, completion: { (action) in
                            
                            if let verifyEmailVC = CLRF_SB.instantiateViewController(withIdentifier: "VerifyEmailViewController") as? VerifyEmailViewController {
                                verifyEmailVC.iObject = param
                                self.present(UINavigationController(rootViewController: verifyEmailVC), animated: true, completion: nil)
                            }
                        })
                    }
                    else {
                        self.showAlertView(meta.valueForString(key: CJsonMessage), completion: nil)
                    }
                }
            }
        }
    }
}
// MARK:-
// MARK:- Helper Method

extension MerchantLoginViewController {
    
    private func attributedSignUp(){
        
        let strSignUp = "NEW TO SMARKET? SIGN UP"
        
        let attributedString = NSMutableAttributedString(string: strSignUp)
        
        attributedString.setAttributes([NSAttributedStringKey.foregroundColor : ColorBlack_000000,
                                        NSAttributedStringKey.font : CFontPoppins(size: IS_iPhone_5 ? 11 : IS_iPhone_6_Plus ? 15 : 13, type: .Regular)], range: NSRange(location: 0, length: strSignUp.count))
        
        attributedString.setAttributes([NSAttributedStringKey.foregroundColor : ColorMerchantAppTheme,
                                        NSAttributedStringKey.font : CFontPoppins(size: IS_iPhone_5 ? 11 : IS_iPhone_6_Plus ? 15 : 13, type: .Regular),
                                        NSAttributedStringKey.underlineStyle : NSUnderlineStyle.styleSingle.rawValue], range:strSignUp.rangeOf("SIGN UP"))
        
        btnSignUp.setAttributedTitle(attributedString, for: .normal)
    }
    
    fileprivate func isValidationPassed() -> Bool {
        
//        if IS_SIMULATOR {
//            return true
//        }
        
        if (txtEmail.text?.isBlank)! {
            self.presentAlertViewWithOneButton(alertTitle: "", alertMessage: CMessageBlankEmail, btnOneTitle: CBtnOk, btnOneTapped: nil)
            return false
        } else if !(txtEmail.text?.isValidEmail)! {
            self.presentAlertViewWithOneButton(alertTitle: "", alertMessage: CMessageValidEmail, btnOneTitle: CBtnOk, btnOneTapped: nil)
            return false
        }
        /*else if (txtPassword.text?.isBlank)! {
            self.presentAlertViewWithOneButton(alertTitle: "", alertMessage: CMessageBlankPassword, btnOneTitle: CBtnOk, btnOneTapped: nil)
            return false
        }
      */  return true
    }
}
