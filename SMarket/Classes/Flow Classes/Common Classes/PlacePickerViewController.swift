//
//  PlacePickerViewController.swift
//  SMarket
//
//  Created by Mac-00014 on 20/07/18.
//  Copyright © 2018 Mind. All rights reserved.
//

import UIKit


class PlacePickerViewController: ParentViewController {
    
    @IBOutlet weak var txtSearch : leftImageTextField!

    //MARK:-
    //MARK:- LIFE CYCLE
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initialize()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        if self.block != nil {
            self.block!(nil,txtSearch.text)
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        
    }
    
    // MARK:-
    // MARK:- GENERAL METHODS
    fileprivate func initialize()  {
        self.title = "Location"
        txtSearch.leftImage = #imageLiteral(resourceName: "location")
        
    }
}
