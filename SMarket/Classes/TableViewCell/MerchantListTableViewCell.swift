//
//  MerchantListTableViewCell.swift
//  SMarket
//
//  Created by Mac-00014 on 02/07/18.
//  Copyright © 2018 Mind. All rights reserved.
//

import UIKit

class MerchantListTableViewCell: UITableViewCell {

    @IBOutlet weak var imgVIcon : UIImageView!
    @IBOutlet weak var lblMerchantName : UILabel!
    @IBOutlet weak var lblSubTitle : UILabel!
    @IBOutlet weak var lblAddress : UILabel!
    @IBOutlet weak var lblDistance : UILabel!
    @IBOutlet weak var lblStoreCredits : UILabel!
    @IBOutlet weak var lblReferrals : UILabel!
    @IBOutlet weak var lblReviews : UILabel!
    @IBOutlet weak var vWRatingV : RatingView!
    
    @IBOutlet weak var btnWeb : UIButton!
    @IBOutlet weak var btnCall : UIButton!
    @IBOutlet weak var btnDirection : UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        btnWeb.tintColor = ColorCustomerAppTheme
        btnCall.tintColor = ColorCustomerAppTheme
        btnDirection.tintColor = ColorCustomerAppTheme
       
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
