//
//  QRCodeDetailsViewController.swift
//  SMarket
//
//  Created by Mac-00014 on 07/07/18.
//  Copyright © 2018 Mind. All rights reserved.
//

import UIKit
enum QRDetailsType   {
    case Offer
    case ReviewOffer
    case StoreCreditOffer
    case ReferOffer
}


class QRCodeDetailsViewController: ParentViewController {
    
    @IBOutlet weak var lblMerchantName : UILabel!
    @IBOutlet weak var lblTagline : UILabel!
    @IBOutlet weak var lblReview : UILabel!
    @IBOutlet weak var lblRatingCount: UILabel!
    @IBOutlet weak var lblExpireDate : UILabel!
    @IBOutlet weak var lblReferredDate : UILabel!
    @IBOutlet weak var lblAvailableStoreCredit : UILabel!
    @IBOutlet weak var lblOfferCode : UILabel!
    @IBOutlet weak var lblCondition : UILabel!
    @IBOutlet weak var lblConditionTitle : UILabel!
    @IBOutlet weak var lblOfferType : UILabel!
    @IBOutlet weak var lblOfferValue : UILabel!
    @IBOutlet weak var lblDistance : UILabel!
    @IBOutlet weak var lblOfferStatus : UILabel!
    
    
    @IBOutlet weak var vWRating : RatingView!
    @IBOutlet weak var imgVQRCode : UIImageView!
    @IBOutlet weak var imgVMerchant : UIImageView!
    @IBOutlet weak var imgVStatusBg : UIImageView!
    @IBOutlet weak var vWAlertView : UIView!
    @IBOutlet weak var vWAlertChildView : UIView!
    @IBOutlet weak var btnOfferType : UIButton!
    @IBOutlet weak var btninvite: UIButton!
    @IBOutlet weak var btnSkip: UIButton!
    
    var qrDetailsType : QRDetailsType!
    
    
    //MARK:-
    //MARK:- LIFE CYCLE
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initialize()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        
    }
    
    // MARK:-
    // MARK:- GENERAL METHODS
    fileprivate func initialize()  {
        
        lblAvailableStoreCredit.text = ""
        
        switch qrDetailsType {
        case .Offer?:
            self.title = "OFFER QR CODE"
            alertPopUpConfiguration()
        case .ReviewOffer?:
            self.title = "RATE & REVIEW QR CODE"
            self.navigationItem.leftBarButtonItem = UIBarButtonItem(image:#imageLiteral(resourceName: "back"), style: .plain, target: self, action: #selector(btnBackClicked))
        case .ReferOffer?:
//            self.presentAlertViewWithTwoButtons(alertTitle: "SMark Offer", alertMessage: CMessageVisiblefriendsAlert, btnOneTitle: CBtnCancel, btnOneTapped: { (sender) in
//                self.popToViewController()
//            }, btnTwoTitle: CBtnProceed, btnTwoTapped: { (sender) in
//
//            })
            self.title = "RATE & REFER QR CODE"
            self.navigationItem.leftBarButtonItem = UIBarButtonItem(image:#imageLiteral(resourceName: "back"), style: .plain, target: self, action: #selector(btnBackClicked))
        case .StoreCreditOffer?:
            self.title = "STORE CREDIT QR CODE"
            lblAvailableStoreCredit.text = "Available store credit: \(currencyUnit)0"
        default :
            self.title = "OFFER QR CODE"
        }
        
        //.. Enable location service and update api
        
        var isNeedRefresh = true
        appDelegate?.enableLocationService({ (status, location) in
            
            if  status == .authorizedWhenInUse && location != nil && isNeedRefresh {
                isNeedRefresh = false
                self.loadOfferDetails()
            }else if status == .restricted || status == .denied{
                isNeedRefresh = true
                self.loadOfferDetails()
            }else {
                self.loadOfferDetails()
            }
        })
        
    }
    
    fileprivate func loadOfferDetails() {
        
        if let dict = iObject as? [String : Any] {
            prefilledData(dict)
        } else {
            if let id = iObject as? String {
                loadOfferDetails(id: id)
            }
        }
    }
    
    fileprivate func prefilledData(_ data:[String : Any]?)  {
        
        if let data = data {
            let offerDetails = OfferDetails(object: data)
            
            imgVMerchant.imageWithUrl(offerDetails.logo)
            imgVMerchant.touchUpInside { (imageView) in
                self.fullScreenImage(imageView, urlString: offerDetails.logo)
            }
            
            lblMerchantName.text = offerDetails.name
            lblTagline.text = offerDetails.tagLine
            lblDistance.text = "(\(offerDetails.distance ?? "0.0") mi)"
            vWRating.setRating((offerDetails.avgRating?.toFloat) ?? 0.0)
            lblRatingCount.text = "\(offerDetails.avgRating?.toFloat ?? 0.0)"
            lblReview.text = "(\(offerDetails.noOfRating ?? "0"))"
            imgVQRCode.imageWithUrl(offerDetails.qrCodeImage)
            lblOfferCode.text = offerDetails.code
            lblExpireDate.textColor = offerDetails.status == .Expire ? ColorRedExpireDate : ColorBlack_000000
            
            if qrDetailsType == .StoreCreditOffer {
                lblAvailableStoreCredit.text = "Available store credit: \(currencyUnit)\(offerDetails.storeCredit ?? "0")"
            }
            
            if let offer = offerDetails.offers {
                
                if let expiryDate = offer.expiryDate?.dateFromString, !expiryDate.isBlank {
                    lblExpireDate.text = "Expires on: \(expiryDate)"
                }
                
                if offer.offerType == .Referral {
                    
                    if let referrerDate = offerDetails.referredDate?.dateFromString, !referrerDate.isBlank {
                        lblReferredDate.text = "Referred on: \(referrerDate)"
                    }
                }
                
                if let subOffer = offer.subOffer?.first {
                    lblOfferType.text = subOffer.categoryName
                    lblCondition.text = subOffer.conditions
                    lblConditionTitle.isHidden = lblCondition.text!.isBlank
                    
                    if subOffer.subOfferCategory == .InStore {
                        lblOfferValue.text = ""
                        btnOfferType.setImage(subOffer.exclusiveImage, for: .normal)
                    }else {
                        lblOfferValue.text = "\(currencyUnit)\(subOffer.amount ?? "0")"
                    }
                } else {
                    btnOfferType.isHidden = offer.subOffer?.first == nil
                    lblConditionTitle.isHidden = true
                }
            } else {
                lblOfferValue.isHidden = true
                btnOfferType.isHidden = true
                lblConditionTitle.isHidden = true
                lblCondition.isHidden = true
            }
            
            switch offerDetails.status {
                
            case .Redeemed:
                lblOfferStatus.textColor = CRGB(r: 37, g: 168, b: 24) // Redeemed
                lblOfferStatus.isHidden = false
                lblOfferStatus.text = "Redeemed"
                imgVStatusBg.isHidden = false
                if let redeemedDate = offerDetails.redeemedDate?.dateFromString, !redeemedDate.isBlank {
                    lblReferredDate.text = "Redeemed on: \(redeemedDate)"
                }
                
            case .Expire:
                lblOfferStatus.textColor = CRGB(r: 247, g: 23, b: 1) // Expire
                lblOfferStatus.isHidden = false
                lblOfferStatus.text = "Expired"
                imgVStatusBg.isHidden = false
                
            default:
                lblOfferStatus.isHidden = true
                imgVStatusBg.isHidden = true
            }
        }
    }
    fileprivate func popToViewController() {
        
        if let viewControllers = self.navigationController?.viewControllers {
            
            if viewControllers[viewControllers.count - 4] is MerchantDetailsViewController {
                if let merchantDetailsVC = viewControllers [viewControllers.count - 4] as? MerchantDetailsViewController {
                    self.navigationController?.popToViewController(merchantDetailsVC, animated: true)
                }
            } else {
                if viewControllers[viewControllers.count - 3] is MerchantDetailsViewController {
                    if let merchantDetailsVC = viewControllers [viewControllers.count - 3] as? MerchantDetailsViewController {
                        self.navigationController?.popToViewController(merchantDetailsVC, animated: true)
                    }
                }
            }
        }
    }
    // MARK:-
    // MARK:- ACTION EVENT
    @objc  fileprivate func btnBackClicked(_ sender : UIBarButtonItem) {
        
        if block != nil {
            self.block!(true,nil)
        }
        popToViewController()
    }
}

// MARK:-
// MARK:- Alert popUp Configuration

extension QRCodeDetailsViewController {
    
    func alertPopUpConfiguration()  {
        
        vWAlertView.CViewSetWidth(width: CScreenWidth)
        vWAlertView.CViewSetHeight(height: CScreenHeight)
        vWAlertChildView.layer.cornerRadius = 5
        vWAlertChildView.layer.masksToBounds = true
        
        self.presentPopUp(view: self.vWAlertView, shouldOutSideClick: false, type: .center) {
            
        }
        
        self.btninvite.touchUpInside(genericTouchUpInsideHandler: { (sender) in
            self.dismissPopUp(view: self.vWAlertView, completionHandler: nil)
            self.checkContactPermission()
        })
        self.btnSkip.touchUpInside(genericTouchUpInsideHandler: { (sender) in
            self.dismissPopUp(view: self.vWAlertView, completionHandler: nil)
        })
    }
}

// MARK:-
// MARK:- Helper Method

extension QRCodeDetailsViewController {
    
    fileprivate func checkContactPermission() {
        
        SwiftyContacts.shared.requestAccess(true) { (granted) in
            
            if granted {
                
                SwiftyContacts.shared.fetchContacts(ContactsSortorder: .givenName, completionHandler: { (result) in
                    
                    switch result{
                    case .Success(response: let contacts):
                        // Do your thing here with [CNContacts] array
                        
                        if let arrContactList = SwiftyContacts.shared.convertContactListToArray(contacts) as? [[String : Any]] {
                            DispatchQueue.main.async {
                                self.sendContactsOnServer(contacts: arrContactList)
                            }
                        }
                        
                    case .Error(error: let error):
                        print(error)
                        break
                    }
                })
            }
        }
    }
}

// MARK:-
// MARK:- Server Request

extension QRCodeDetailsViewController {
    
    fileprivate func loadOfferDetails(id : String) {
        
        var param = [String : Any]()
        param["id"] = id
        
        if (iObject as? [String : Any]) == nil {
            self.view.startLoadingAnimation(tintColor: .gray, backgroundColor: .white)
        }
        
        APIRequest.shared().loadOfferDetails(param) { (response, error) in
            
            if APIRequest.shared().isJSONDataValid(withResponse: response) {
                if let dataResponse = response as? [String : Any], let data = dataResponse[CJsonData] as? [String: Any] {
                    self.iObject = data
                    self.prefilledData(data)
                }
            }
            
            //For remove loader or display data not found
            if (self.iObject as? [String : Any]) != nil  {
                self.view.stopLoadingAnimation()
                
            } else if error == nil {
                self.view.showDataStatusView(status: .noResultFound, tintColor: .gray, backgroundColor: .clear, tapToRetry: nil)
                
            } else if let error = error as NSError? {
                
                // ... -999 cancelled api
                // ... --1001 or -1009 no internet connection
                
                if error.code != -999 {
                    
                    self.view.showDataStatusView(status: (error.code == -1001 || error.code == -1009) ? .noInternet : .other, tintColor: .gray, backgroundColor: .clear, tapToRetry: {
                        if let offerId = self.iObject as? String {
                            self.loadOfferDetails(id: offerId)
                        }
                    })
                }
            }
        }
    }
    
    fileprivate func sendContactsOnServer(contacts : [Any]){
        
        var param = [String : Any]()
        param["contact_list"] =  contacts
        
        APIRequest.shared().sendContactsOnServer(param) { (response, error) in
            
            if APIRequest.shared().isJSONStatusValid(withResponse: response) {
                
                if let json = response as? [String : Any], let data = json[CJsonData] as? [String : Any] {
                    var param = [String : Any]()
                    param["smarket_contact_list"] = data["smarket_contact_list"]
                    
                    if let arrOtherContact = data["other_contact_list"] as? [Any], arrOtherContact.count > 0 {
                        if let friendListVC = CMainCustomer_SB.instantiateViewController(withIdentifier: "FriendListVC") as? FriendListVC {
                            friendListVC.navigation = .rewards
                            friendListVC.iObject = contacts
                            friendListVC.params = param
                            self.navigationController?.pushViewController(friendListVC, animated: true)
                        }
                    }
                }
            }
        }
    }
}
