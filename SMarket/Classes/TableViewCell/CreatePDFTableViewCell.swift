//
//  CreatePDFTableViewCell.swift
//  SMarket
//
//  Created by Mac-00014 on 19/07/18.
//  Copyright © 2018 Mind. All rights reserved.
//

import UIKit

class CreatePDFTableViewCell: UITableViewCell {
    
    @IBOutlet weak var btnCreatePDF : UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
