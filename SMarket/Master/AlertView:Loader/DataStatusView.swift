//
//  DataStatusView.swift
//  StudyBuddy
//
//  Created by mac-0007 on 17/04/18.
//  Copyright © 2018 mac-0007. All rights reserved.
//

import UIKit

class DataStatusView: UIView {

    @IBOutlet weak var imgVStatus: UIImageView!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var btnRetry: UIButton!
    
}
